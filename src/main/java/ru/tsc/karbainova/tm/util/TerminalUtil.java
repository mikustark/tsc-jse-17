package ru.tsc.karbainova.tm.util;

import ru.tsc.karbainova.tm.exception.AbstractException;
import ru.tsc.karbainova.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {
    Scanner scanner = new Scanner(System.in);

    static String nextLine() {
        return scanner.nextLine();
    }

    static Integer nextNumber() throws AbstractException {
        final String value = scanner.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }
}
